﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;
using System.IO;
using TechTalk.SpecFlow;

namespace DynamicLoading
{
    [Binding]
    public class DynamicLoadingPage
    {
        public static IWebDriver Instance { get; set; }
   
        [BeforeScenario]
        public void Initialize()
        {
            if (Instance == null)
            {
                ChromeOptions chOptions = new ChromeOptions();
                var service = ChromeDriverService.CreateDefaultService(@GetDriverPath());
                Instance = new ChromeDriver(service, chOptions, TimeSpan.FromSeconds(120));
                Instance.Manage().Window.Maximize();
            }
            else
            {
                Instance.Manage().Cookies.DeleteAllCookies();
            }            
        }

        public string VerifyText()
        {
           
            return null;
        }

        public void ClickOnStart()
        {
            // 
            
        }
        public void NavigateToUrl(string url)
        {
            Instance.Navigate().GoToUrl(url);
        }

        private static string GetDriverPath()
        {
            var requiredPath = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
            requiredPath = requiredPath?.Replace("file:\\", "") + "\\drivers";
            Trace.WriteLine(requiredPath);
            return requiredPath;
        }

        [AfterTestRun]
        public static void CloseDriver()
        {
            Instance.Close();
        }
    }
}

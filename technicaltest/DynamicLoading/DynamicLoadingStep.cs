﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace DynamicLoading
{

   public class DynamicLoadingStep : DynamicLoadingPage
    {

        DynamicLoadingPage dynamicloadingpage= new DynamicLoadingPage();

        [Given(@"I navigate to ""(.*)""")]
        public void GivenINavigateTo(string url)
        {
            dynamicloadingpage.NavigateToUrl(url);
        }

        [When(@"I click on start button")]
        public void WhenIClickOnStartButton()
        {
            // use the existing method to click on element
        }

        [Then(@"I should see the text as ""(.*)""")]
        public void ThenIShouldSeeTheTextAs(string text)
        {
            // write the assertion to verify the text
         
        
        }

    }
}
